package test.tide.com.tidetest;

import android.net.Uri;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import test.tide.com.data.entity.Place;
import test.tide.com.data.mocks.MockDataStore;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by Lucian on 1/11/2017.
 */

@RunWith(AndroidJUnit4.class)
public class BarsViewInstrumentedTest {
    @Rule
    public IntentsTestRule<MainActivity> rule = new IntentsTestRule<>(MainActivity.class);

    private List<Place> mockedBars;

    @Before
    public void setup() {
        mockedBars = new MockDataStore().mockedPlaces();
    }

    @Test
    public void testItemsDisplayed() {
        onView(allOf(
                withText(R.string.list),
                isDescendantOfA(withId(R.id.tabs))
        )).perform(click());

        for (Place place : mockedBars) {
            onView(withText(place.getName()))
                    .check(matches(isDisplayed()));
        }
    }

    @Test
    public void testGoogleMapsOpened() {
        onView(allOf(
                withText(R.string.list),
                isDescendantOfA(withId(R.id.tabs))
        )).perform(click());

        Place bar = mockedBars.get(0);
        onView(withText(bar.getName()))
                .perform(click());

        intended(allOf(
                toPackage("com.google.android.apps.maps"),
                hasData(Uri.parse("geo:" + bar.getGeometry().getLocation().getLatitude() + ","
                        + bar.getGeometry().getLocation().getLongitude()
                        + "?q=" + Uri.encode(bar.getName())))
        ));
    }
}
