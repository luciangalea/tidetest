package test.tide.com.tidetest;

import android.app.Application;
import android.content.Context;
import android.support.test.runner.AndroidJUnitRunner;

import test.tide.com.tidetest.di.MockApplication;

/**
 * Created by Lucian on 1/11/2017.
 */

public class MockTestRunner extends AndroidJUnitRunner {
    @Override
    public Application newApplication(ClassLoader cl, String className, Context context) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return super.newApplication(cl, MockApplication.class.getName(), context);
    }
}
