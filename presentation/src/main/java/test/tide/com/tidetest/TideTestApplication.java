package test.tide.com.tidetest;

import android.app.Application;

import dagger.internal.DaggerCollections;
import test.tide.com.tidetest.di.ApplicationComponent;
import test.tide.com.tidetest.di.ApplicationModule;
import test.tide.com.tidetest.di.DaggerApplicationComponent;
import test.tide.com.tidetest.di.DataModule;

/**
 * Created by Lucian on 1/11/2017.
 */

public class TideTestApplication extends Application {
    private ApplicationComponent applicationComponent;

    public ApplicationComponent getApplicationComponent() {
        if (applicationComponent == null) {
            applicationComponent = createApplicationComponent();
        }
        return applicationComponent;
    }

    protected ApplicationComponent createApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataModule(new DataModule())
                .build();
    }
}
