package test.tide.com.tidetest.view;

import android.location.Location;

import java.util.List;

import test.tide.com.data.entity.Place;

/**
 * Created by Lucian on 1/11/2017.
 */

public interface BarsView {
    void renderBars(Location location, List<Place> bars);

    void renderError();

    void showProgress();

    void hideProgress();

    void navigateToGoogleMapsApp(double latitude, double longitude, String name);
}
