package test.tide.com.tidetest.di;

import test.tide.com.tidetest.TideTestApplication;

/**
 * Created by Lucian on 1/11/2017.
 */

public class MockApplication extends TideTestApplication {
    @Override
    protected ApplicationComponent createApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataModule(new MockDataModule())
                .build();
    }
}
