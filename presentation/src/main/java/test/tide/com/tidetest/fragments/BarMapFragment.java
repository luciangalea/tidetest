package test.tide.com.tidetest.fragments;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import test.tide.com.data.entity.Place;
import test.tide.com.domain.location.DistanceConverter;
import test.tide.com.tidetest.TideTestApplication;

/**
 * Created by Lucian on 1/10/2017.
 */

public class BarMapFragment extends SupportMapFragment implements OnMapReadyCallback {
    private static final String ARG_BARS = "argBars";
    private static final String ARG_LOCATION = "argLocation";

    private GoogleMap googleMap;
    private List<Place> bars;
    private Location location;

    @Inject
    DistanceConverter distanceConverter;

    public static BarMapFragment newInstance(List<Place> bars, Location location) {
        Bundle args = new Bundle();
        if (bars != null && location != null) {
            Place[] barsArray = new Place[bars.size()];
            bars.toArray(barsArray);
            args.putParcelableArray(ARG_BARS, barsArray);
            args.putParcelable(ARG_LOCATION, location);
        }
        BarMapFragment fragment = new BarMapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);

        Bundle arguments = getArguments();
        if (arguments != null) {
            Parcelable[] parcelables = arguments.getParcelableArray(ARG_BARS);
            if (parcelables != null) {
                Place[] bars = new Place[parcelables.length];
                System.arraycopy(parcelables, 0, bars, 0, bars.length);
                this.bars = Arrays.asList(bars);
            }
            location = arguments.getParcelable(ARG_LOCATION);
        }
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        getMapAsync(this);
        ((TideTestApplication) getContext().getApplicationContext()).getApplicationComponent().inject(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (bars != null && location != null) {
            renderBarsAndLocation();
        }
    }

    public void setBarsAndLocation(List<Place> bars, Location location) {
        this.bars = bars;
        this.location = location;
        if (googleMap != null) {
            renderBarsAndLocation();
        }
    }

    private void renderBarsAndLocation() {
        LatLng userPosition = new LatLng(location.getLatitude(), location.getLongitude());
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userPosition, 12));

        for (Place bar : bars) {
            LatLng barPosition = new LatLng(bar.getGeometry().getLocation().getLatitude(),
                    bar.getGeometry().getLocation().getLongitude());
            googleMap.addMarker(new MarkerOptions().position(barPosition)
                    .title(bar.getName())
                    .snippet(distanceConverter.computeDistance(location, bar)));
        }
    }
}
