package test.tide.com.tidetest.di;

import android.content.Context;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import test.tide.com.data.cache.Cache;
import test.tide.com.data.cache.MemoryCache;
import test.tide.com.data.store.CloudDataStore;
import test.tide.com.data.store.DataStore;
import test.tide.com.data.store.GooglePlacesAPI;
import test.tide.com.tidetest.R;

/**
 * Created by Lucian on 1/11/2017.
 */

@Module
public class DataModule {
    @Provides
    DataStore providesDataStore(Context context) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(context.getString(R.string.base_url))
                .build();
        return new CloudDataStore(retrofit.create(GooglePlacesAPI.class));
    }

    @Provides
    @Named("memory")
    Cache providesMemoryCache() {
        return new MemoryCache();
    }
}
