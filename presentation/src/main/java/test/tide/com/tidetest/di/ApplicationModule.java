package test.tide.com.tidetest.di;

import android.app.Application;
import android.content.Context;

import com.patloew.rxlocation.RxLocation;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import test.tide.com.data.store.DataStore;
import test.tide.com.domain.location.DistanceConverter;
import test.tide.com.domain.location.LocationProvider;
import test.tide.com.domain.location.LocationProviderImpl;
import test.tide.com.domain.location.MilesDistanceConverterImpl;
import test.tide.com.domain.location.SearchConfiguration;
import test.tide.com.domain.location.SearchConfigurationImpl;
import test.tide.com.domain.thread.RxThread;
import test.tide.com.tidetest.R;

/**
 * Created by Lucian on 1/11/2017.
 */

@Module
public class ApplicationModule {
    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    Context providesContext() {
        return application;
    }

    @Provides
    @Named("observeThread")
    RxThread providesObserveThread() {
        return AndroidSchedulers::mainThread;
    }

    @Provides
    @Named("subscribeThread")
    RxThread providesSubscribeTread() {
        return Schedulers::io;
    }

    @Provides
    @Singleton
    DistanceConverter providesDistanceConverter(Context context) {
        return new MilesDistanceConverterImpl(context.getString(R.string.distance_format));
    }

    @Provides
    @Singleton
    SearchConfiguration providesSearchConfiguration(Context context) {
        return new SearchConfigurationImpl(context.getString(R.string.api_key), "bar");
    }

    @Provides
    @Singleton
    LocationProvider providesLocationProvider(Context context,
                                              @Named("observeThread") RxThread observeThread,
                                              @Named("subscribeThread") RxThread subcribeThread,
                                              SearchConfiguration searchConfiguration) {
        RxLocation rxLocation = new RxLocation(context);
        rxLocation.setDefaultTimeout(searchConfiguration.getTimeoutInSeconds(), TimeUnit.SECONDS);

        return new LocationProviderImpl(rxLocation, subcribeThread, observeThread);
    }
}
