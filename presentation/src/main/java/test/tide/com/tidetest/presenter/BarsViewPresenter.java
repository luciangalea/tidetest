package test.tide.com.tidetest.presenter;

import android.location.Location;
import android.support.v4.util.Pair;

import com.google.android.gms.location.LocationRequest;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import test.tide.com.data.entity.Place;
import test.tide.com.domain.interactors.GetPlacesInteractor;
import test.tide.com.domain.location.LocationProvider;
import test.tide.com.domain.location.SearchConfiguration;
import test.tide.com.tidetest.view.BarsView;

/**
 * Created by Lucian on 1/11/2017.
 */

public class BarsViewPresenter {
    private BarsView view;
    private Disposable locationDisposable;
    private LocationRequest locationRequest;
    private SearchConfiguration searchConfiguration;

    private LocationProvider locationProvider;
    private GetPlacesInteractor getPlacesInteractor;
    private Location currentLocation;

    @Inject
    public BarsViewPresenter(LocationProvider locationProvider, SearchConfiguration searchConfiguration, GetPlacesInteractor getPlacesInteractor) {
        this.locationProvider = locationProvider;
        this.searchConfiguration = searchConfiguration;
        this.getPlacesInteractor = getPlacesInteractor;

        this.locationRequest = LocationRequest.create()
                .setPriority(searchConfiguration.getPriority())
                .setInterval(searchConfiguration.getInterval());
    }

    public void attachView(BarsView view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
        if (this.locationDisposable != null && !this.locationDisposable.isDisposed()) {
            this.locationDisposable.dispose();
        }
    }

    public void onLocationPermissionsGranted() {
        LocationRequest request = new LocationRequest();
        locationDisposable = locationProvider.checkLocationSettings(request)
                .flattenAsObservable(Collections::singletonList)
                .flatMap(this::listenForLocation)
                .subscribe(this::onPlacesRetrieved, this::onPlacesRetrievalError);
    }

    public void onBarSelected(Place bar) {
        view.navigateToGoogleMapsApp(bar.getGeometry().getLocation().getLatitude(),
                bar.getGeometry().getLocation().getLongitude(),
                bar.getName());
    }

    private Observable<Pair<Location, List<Place>>> listenForLocation(boolean locationSettingsEnabled) {
        if (locationSettingsEnabled) {
            return this.locationProvider.retrieveLocation(locationRequest)
                    .filter(location -> currentLocation == null ||
                            currentLocation.distanceTo(location) > searchConfiguration.getThreshold())
                    .doOnNext(location -> this.currentLocation = location)
                    .flatMap(this::getPlaces);
        } else {
            return this.locationProvider.getLastLocation()
                    .flattenAsObservable(Collections::singletonList)
                    .flatMap(this::getPlaces);
        }
    }

    private Observable<Pair<Location, List<Place>>> getPlaces(Location location) {
        view.showProgress();
        return this.getPlacesInteractor
                .withData(location, searchConfiguration.getRadius(),
                        searchConfiguration.getType(), searchConfiguration.getApiKey())
                .execute();
    }

    private void onPlacesRetrieved(Pair<Location, List<Place>> data) {
        view.renderBars(data.first, data.second);
        view.hideProgress();
    }

    private void onPlacesRetrievalError(Throwable throwable) {
        view.renderError();
        view.hideProgress();
    }
}
