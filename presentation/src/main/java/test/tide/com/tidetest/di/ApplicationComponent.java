package test.tide.com.tidetest.di;

import javax.inject.Singleton;

import dagger.Component;
import test.tide.com.tidetest.MainActivity;
import test.tide.com.tidetest.fragments.BarListFragment;
import test.tide.com.tidetest.fragments.BarMapFragment;

/**
 * Created by Lucian on 1/11/2017.
 */

@Component(modules = {ApplicationModule.class, DataModule.class})
@Singleton
public interface ApplicationComponent {
    void inject(MainActivity mainActivity);

    void inject(BarListFragment fragment);

    void inject(BarMapFragment fragment);
}
