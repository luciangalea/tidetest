package test.tide.com.tidetest.fragments;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.tide.com.data.entity.Place;
import test.tide.com.tidetest.R;
import test.tide.com.tidetest.TideTestApplication;
import test.tide.com.tidetest.adapter.BarsAdapter;

/**
 * Created by Lucian on 1/10/2017.
 */

public class BarListFragment extends Fragment {
    private static final String ARG_BARS = "argBars";
    private static final String ARG_LOCATION = "argLocation";

    private List<Place> bars;
    private Location location;

    @Inject
    BarsAdapter barsAdapter;
    @BindView(R.id.recycler_bars)
    RecyclerView barsRecyclerView;

    public static BarListFragment newInstance(List<Place> bars, Location location) {
        Bundle args = new Bundle();
        if (bars != null && location != null) {
            Place[] barsArray = new Place[bars.size()];
            bars.toArray(barsArray);
            args.putParcelableArray(ARG_BARS, barsArray);
            args.putParcelable(ARG_LOCATION, location);
        }
        BarListFragment fragment = new BarListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null) {
            Parcelable[] parcelables = arguments.getParcelableArray(ARG_BARS);
            if (parcelables != null) {
                Place[] bars = new Place[parcelables.length];
                System.arraycopy(parcelables, 0, bars, 0, bars.length);
                this.bars = Arrays.asList(bars);
            }
            location = arguments.getParcelable(ARG_LOCATION);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bar_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this,  view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((TideTestApplication) getContext().getApplicationContext()).getApplicationComponent().inject(this);
        if (getActivity() instanceof BarsAdapter.BarSelectedListener) {
            barsAdapter.setListener((BarsAdapter.BarSelectedListener) getActivity());
        }
        barsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        barsRecyclerView.setAdapter(barsAdapter);

        if (bars != null && location != null) {
            renderBars(bars, location);
        }
    }

    public void setBarsAndLocation(List<Place> bars, Location location) {
        this.bars = bars;
        this.location = location;
        //view may not be inflated yet - if this is the case, the list will be rendered after inflation is complete
        if (getView() != null && barsAdapter != null) {
            renderBars(bars, location);
        }
    }

    private void renderBars(List<Place> bars, Location location) {
        barsAdapter.setBarsAndLocation(bars, location);
        barsAdapter.notifyDataSetChanged();
    }
}
