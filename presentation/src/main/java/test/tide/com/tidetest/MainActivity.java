package test.tide.com.tidetest;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.tide.com.data.entity.Place;
import test.tide.com.tidetest.adapter.BarsAdapter;
import test.tide.com.tidetest.fragments.BarListFragment;
import test.tide.com.tidetest.fragments.BarMapFragment;
import test.tide.com.tidetest.presenter.BarsViewPresenter;
import test.tide.com.tidetest.view.BarsView;

public class MainActivity extends AppCompatActivity implements BarsView, BarsAdapter.BarSelectedListener {

    public static final int REQUEST_LOCATION_PERMISSION = 1000;
    private SectionsPagerAdapter pagerAdapter;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.main_content)
    ViewGroup mainViewGroup;

    private List<Place> currentBars;
    private Location currentLocation;
    private boolean permissionsWereRequested;

    @Inject
    BarsViewPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        ButterKnife.bind(this);
        ((TideTestApplication) getApplication()).getApplicationComponent().inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkPlayServicesAvailable()) {
            if (!permissionsWereRequested && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionsWereRequested = true;
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION_PERMISSION);
            } else {
                presenter.onLocationPermissionsGranted();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detachView();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                presenter.onLocationPermissionsGranted();
            } else {
                Snackbar.make(mainViewGroup, getString(R.string.app_requires_location_for_displaying_nearby_bars), Snackbar.LENGTH_INDEFINITE).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean checkPlayServicesAvailable() {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int status = apiAvailability.isGooglePlayServicesAvailable(this);

        if (status != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(status)) {
                apiAvailability.getErrorDialog(this, status, 1).show();
            } else {
                Snackbar.make(mainViewGroup, getString(R.string.google_play_services_error), Snackbar.LENGTH_INDEFINITE).show();
            }
            return false;
        }
        return true;
    }

    @Override
    public void renderBars(Location location, List<Place> bars) {
        this.currentLocation = location;
        this.currentBars = bars;
        if (pagerAdapter.barListFragment != null) {
            pagerAdapter.barListFragment.setBarsAndLocation(currentBars, currentLocation);
        }
        if (pagerAdapter.barMapFragment != null) {
            pagerAdapter.barMapFragment.setBarsAndLocation(currentBars, currentLocation);
        }
    }

    @Override
    public void renderError() {

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBarSelected(Place bar) {
        presenter.onBarSelected(bar);
    }

    @Override
    public void navigateToGoogleMapsApp(double latitude, double longitude, String name) {
        Uri mapsIntentUri = Uri.parse("geo:" + latitude + "," + longitude + "?q=" + Uri.encode(name));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapsIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            Toast.makeText(this, getString(R.string.google_maps_not_available), Toast.LENGTH_LONG).show();
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        private BarListFragment barListFragment;
        private BarMapFragment barMapFragment;

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return BarListFragment.newInstance(currentBars, currentLocation);
            } else {
                return BarMapFragment.newInstance(currentBars, currentLocation);
            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Object object = super.instantiateItem(container, position);
            if (position == 0 && object instanceof BarListFragment) {
                barListFragment = (BarListFragment) object;
            } else if (position == 1 && object instanceof BarMapFragment) {
                barMapFragment = (BarMapFragment) object;
            }
            return object;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.list);
                case 1:
                    return getString(R.string.map);
            }
            return null;
        }
    }
}
