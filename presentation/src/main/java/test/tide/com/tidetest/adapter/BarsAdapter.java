package test.tide.com.tidetest.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import test.tide.com.data.entity.Place;
import test.tide.com.domain.location.DistanceConverter;
import test.tide.com.tidetest.BR;
import test.tide.com.tidetest.R;

/**
 * Created by Lucian on 1/11/2017.
 */

public class BarsAdapter extends RecyclerView.Adapter<BarsAdapter.ViewHolder> {
    private List<Place> bars;
    private Location location;

    private DistanceConverter distanceConverter;
    private BarSelectedListener listener;

    @Inject
    BarsAdapter(DistanceConverter distanceConverter) {
        this.distanceConverter = distanceConverter;
    }

    public void setListener(BarSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public BarsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_restaurant, parent, false));
    }

    @Override
    public void onBindViewHolder(BarsAdapter.ViewHolder holder, int position) {
        Place bar = bars.get(position);
        holder.viewDataBinding.setVariable(BR.bar, bar);
        holder.viewDataBinding.executePendingBindings();
        holder.barDistanceTextView.setText(distanceConverter.computeDistance(location, bar));
    }

    @Override
    public int getItemCount() {
        return bars != null ? bars.size() : 0;
    }

    public void setBarsAndLocation(List<Place> bars, Location location) {
        this.bars = bars;
        this.location = location;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding viewDataBinding;
        TextView barDistanceTextView;

        ViewHolder(View itemView) {
            super(itemView);

            viewDataBinding = DataBindingUtil.bind(itemView);
            barDistanceTextView = (TextView) itemView.findViewById(R.id.txt_bar_distance);
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onBarSelected(bars.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface BarSelectedListener {
        void onBarSelected(Place bar);
    }
}
