package test.tide.com.tidetest.di;

import android.content.Context;

import test.tide.com.data.mocks.MockDataStore;
import test.tide.com.data.store.DataStore;

/**
 * Created by Lucian on 1/11/2017.
 */

public class MockDataModule extends DataModule {
    @Override
    DataStore providesDataStore(Context context) {
        return new MockDataStore();
    }
}
