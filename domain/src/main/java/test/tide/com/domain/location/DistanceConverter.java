package test.tide.com.domain.location;

import android.location.Location;

import test.tide.com.data.entity.Place;

/**
 * Created by Lucian on 1/11/2017.
 */

public interface DistanceConverter {
    String computeDistance(Location location, Place place);
}
