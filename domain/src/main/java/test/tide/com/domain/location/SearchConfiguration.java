package test.tide.com.domain.location;

/**
 * Created by Lucian on 1/11/2017.
 */

public interface SearchConfiguration {
    double getRadius();

    int getPriority();

    long getInterval();

    String getType();

    String getApiKey();

    int getTimeoutInSeconds();

    //distance in meters between the locations that will trigger a new request for places
    long getThreshold();
}
