package test.tide.com.domain.location;

import android.location.Location;

import com.google.android.gms.location.LocationRequest;
import com.patloew.rxlocation.RxLocation;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import test.tide.com.domain.thread.RxThread;

/**
 * Created by Lucian on 1/11/2017.
 */

public class LocationProviderImpl implements LocationProvider {
    private RxLocation rxLocation;
    private RxThread subscribeThread;
    private RxThread observeThread;


    public LocationProviderImpl(RxLocation rxLocation, RxThread subscribeThread, RxThread observeThread) {
        this.rxLocation = rxLocation;
        this.subscribeThread = subscribeThread;
        this.observeThread = observeThread;
    }

    @Override
    public Single<Boolean> checkLocationSettings(LocationRequest request) {
        return rxLocation.settings().checkAndHandleResolution(request)
                .subscribeOn(subscribeThread.getScheduler())
                .observeOn(observeThread.getScheduler());
    }

    @Override
    public Observable<Location> retrieveLocation(LocationRequest request) {
        return rxLocation.location().updates(request)
                .subscribeOn(subscribeThread.getScheduler())
                .observeOn(observeThread.getScheduler());
    }

    @Override
    public Maybe<Location> getLastLocation() {
        return rxLocation.location().lastLocation()
                .subscribeOn(subscribeThread.getScheduler())
                .observeOn(observeThread.getScheduler());
    }
}
