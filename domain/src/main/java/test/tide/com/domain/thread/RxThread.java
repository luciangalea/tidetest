package test.tide.com.domain.thread;

import io.reactivex.Scheduler;

/**
 * Created by Lucian on 1/11/2017.
 */

public interface RxThread {
    Scheduler getScheduler();
}
