package test.tide.com.domain.location;

import android.location.Location;

import com.google.android.gms.location.LocationRequest;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by Lucian on 1/11/2017.
 */

public interface LocationProvider {
    Single<Boolean> checkLocationSettings(LocationRequest request);

    Observable<Location> retrieveLocation(LocationRequest request);

    Maybe<Location> getLastLocation();
}
