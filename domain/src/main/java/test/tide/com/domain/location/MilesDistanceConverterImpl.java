package test.tide.com.domain.location;

import android.location.Location;

import test.tide.com.data.entity.Place;

/**
 * Created by Lucian on 1/11/2017.
 */

public class MilesDistanceConverterImpl implements DistanceConverter {
    private String distanceFormat;

    public MilesDistanceConverterImpl(String distanceFormat) {
        this.distanceFormat = distanceFormat;
    }

    @Override
    public String computeDistance(Location location, Place place) {
        Location placeLocation = new Location("placeLocation");
        placeLocation.setLatitude(place.getGeometry().getLocation().getLatitude());
        placeLocation.setLongitude(place.getGeometry().getLocation().getLongitude());

        double meters = location.distanceTo(placeLocation);
        return String.format(distanceFormat, meters * 0.000621371192);
    }
}
