package test.tide.com.domain.interactors;

import android.location.Location;
import android.support.v4.util.Pair;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import test.tide.com.data.entity.Place;
import test.tide.com.data.repository.PlacesRepository;
import test.tide.com.domain.thread.RxThread;

/**
 * Created by Lucian on 1/11/2017.
 */

public class GetPlacesInteractor extends BaseInteractor<Pair<Location, List<Place>>> {
    private PlacesRepository repository;

    private Location location;
    private double radius;
    private String type;
    private String apiKey;

    @Inject
    public GetPlacesInteractor(@Named("subscribeThread") RxThread subscribeThread,
                               @Named("observeThread") RxThread observeThread,
                               PlacesRepository repository) {
        super(subscribeThread, observeThread);

        this.repository = repository;
    }

    public GetPlacesInteractor withData(Location location, double radius, String type, String apiKey) {
        this.location = location;
        this.radius = radius;
        this.type = type;
        this.apiKey = apiKey;
        return this;
    }

    @Override
    public Observable<Pair<Location, List<Place>>> operation() {
        return repository.getPlaces(location.getLatitude(), location.getLongitude(), radius, type, apiKey)
                .map(places -> new Pair<>(location, places));
    }
}
