package test.tide.com.domain.location;

import com.google.android.gms.location.LocationRequest;

/**
 * Created by Lucian on 1/11/2017.
 */

public class SearchConfigurationImpl implements SearchConfiguration {
    private String apiKey;
    private String type;

    public SearchConfigurationImpl(String apiKey, String type) {
        this.apiKey = apiKey;
        this.type = type;
    }

    @Override
    public double getRadius() {
        return 1000;
    }

    @Override
    public int getPriority() {
        return LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
    }

    @Override
    public long getInterval() {
        return 30000;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getApiKey() {
        return apiKey;
    }

    @Override
    public int getTimeoutInSeconds() {
        return 15;
    }

    @Override
    public long getThreshold() {
        return 100;
    }
}
