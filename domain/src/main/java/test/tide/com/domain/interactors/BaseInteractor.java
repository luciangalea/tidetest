package test.tide.com.domain.interactors;

import io.reactivex.Observable;
import test.tide.com.domain.thread.RxThread;

/**
 * Created by Lucian on 1/11/2017.
 */

public abstract class BaseInteractor<T> {
    private RxThread subscribeThread;
    private RxThread observeThread;

    public BaseInteractor(RxThread subscribeThread, RxThread observeThread) {
        this.subscribeThread = subscribeThread;
        this.observeThread = observeThread;
    }

    public Observable<T> execute() {
        return operation()
                .subscribeOn(subscribeThread.getScheduler())
                .observeOn(observeThread.getScheduler());
    }

    public abstract Observable<T> operation();
}
