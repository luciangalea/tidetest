package test.tide.com.data.store;

import java.util.List;

import io.reactivex.Observable;
import test.tide.com.data.entity.GetPlacesResult;
import test.tide.com.data.entity.Place;

/**
 * Created by Lucian on 1/10/2017.
 */

public class CloudDataStore implements DataStore {
    private GooglePlacesAPI api;

    public CloudDataStore(GooglePlacesAPI api) {
        this.api = api;
    }


    @Override
    public Observable<GetPlacesResult> fetchPlaces(double latitude, double longitude, final double radius, final String type, final String apiKey) {
        final String location = latitude + "," + longitude;
        return api.getPlaces(location, radius, type, apiKey);
    }
}
