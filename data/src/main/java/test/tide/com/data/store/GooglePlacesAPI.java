package test.tide.com.data.store;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import test.tide.com.data.entity.GetPlacesResult;
import test.tide.com.data.entity.Place;

/**
 * Created by Lucian on 1/10/2017.
 */

public interface GooglePlacesAPI {
    @GET("/maps/api/place/nearbysearch/json")
    Observable<GetPlacesResult> getPlaces(@Query("location") String location, @Query("radius") double radius, @Query("type") String type, @Query("key") String apiKey);
}
