package test.tide.com.data.cache;

import java.util.List;

import test.tide.com.data.entity.Place;

/**
 * Created by Lucian on 1/11/2017.
 */

public interface Cache {
    List<Place> getPlaces(double latitude, double longitude, double radius, String type);

    void storePlaces(double latitude, double longitude, double radius, String type, List<Place> places);
}
