package test.tide.com.data.repository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import io.reactivex.Observable;
import test.tide.com.data.cache.Cache;
import test.tide.com.data.entity.GetPlacesResult;
import test.tide.com.data.entity.Place;
import test.tide.com.data.store.DataStore;

/**
 * Created by Lucian on 1/11/2017.
 */

@Singleton
public class PlacesRepository {
    private DataStore dataStore;
    private Cache cache;

    @Inject
    public PlacesRepository(DataStore dataStore, @Named("memory") Cache cache) {
        this.dataStore = dataStore;
        this.cache = cache;
    }

    public Observable<List<Place>> getPlaces(final double latitude, final double longitude, final double radius, final String type, final String apiKey) {
        final List<Place> cachedPlaces = cache.getPlaces(latitude, longitude, radius, type);
        if (cachedPlaces != null) {
            return Observable.just(cachedPlaces);
        } else {
            return dataStore.fetchPlaces(latitude, longitude, radius, type, apiKey)
                    .map(GetPlacesResult::getPlaces)
                    .doOnNext(places -> cache.storePlaces(latitude, longitude, radius, type, places));
        }
    }
}
