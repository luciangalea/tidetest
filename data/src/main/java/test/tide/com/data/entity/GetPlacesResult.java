package test.tide.com.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Lucian on 1/11/2017.
 */

public class GetPlacesResult {
    @SerializedName("results")
    private List<Place> places;

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }
}
