package test.tide.com.data.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import test.tide.com.data.entity.Place;

/**
 * Created by Lucian on 1/11/2017.
 */

public class MemoryCache implements Cache {
    private Map<QueryParams, List<Place>> cachedPlaces = new HashMap<>();

    @Override
    public List<Place> getPlaces(double latitude, double longitude, double radius, String type) {
        return cachedPlaces.get(new QueryParams(latitude, longitude, radius, type));
    }

    @Override
    public void storePlaces(double latitude, double longitude, double radius, String type, List<Place> places) {
        cachedPlaces.put(new QueryParams(latitude, longitude, radius, type), places);
    }

    private static class QueryParams {
        private double latitude;
        private double longitude;
        private double radius;
        private String type;

        public QueryParams(double latitude, double longitude, double radius, String type) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.radius = radius;
            this.type = type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            QueryParams queryParams = (QueryParams) o;

            if (Double.compare(queryParams.latitude, latitude) != 0) return false;
            if (Double.compare(queryParams.longitude, longitude) != 0) return false;
            if (Double.compare(queryParams.radius, radius) != 0) return false;
            return type != null ? type.equals(queryParams.type) : queryParams.type == null;

        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            temp = Double.doubleToLongBits(latitude);
            result = (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(longitude);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(radius);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            result = 31 * result + (type != null ? type.hashCode() : 0);
            return result;
        }
    }
}
