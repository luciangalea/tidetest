package test.tide.com.data.mocks;

import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import test.tide.com.data.entity.Geometry;
import test.tide.com.data.entity.GetPlacesResult;
import test.tide.com.data.entity.Location;
import test.tide.com.data.entity.Place;
import test.tide.com.data.store.DataStore;

/**
 * Created by Lucian on 1/11/2017.
 */

public class MockDataStore implements DataStore {
    @Override
    public Observable<GetPlacesResult> fetchPlaces(double latitude, double longitude, double radius, String type, String apiKey) {
        GetPlacesResult result = new GetPlacesResult();
        result.setPlaces(mockedPlaces());
        return Observable.just(result);
    }

    @NonNull
    public List<Place> mockedPlaces() {
        return Arrays.asList(
                createPlace("Mock1", 30, 30),
                createPlace("Mock2", 30.1, 30.1),
                createPlace("Mock3", 30.2, 30.2),
                createPlace("Mock4", 30.2, 30.2),
                createPlace("Mock5", 30.3, 30.3)
        );
    }

    private Place createPlace(String name, double latitude, double longitude) {
        Location location = new Location();
        location.setLatitude(latitude);
        location.setLongitude(longitude);

        Geometry geometry = new Geometry();
        geometry.setLocation(location);

        Place place = new Place();
        place.setName(name);
        place.setGeometry(geometry);
        return place;
    }
}
