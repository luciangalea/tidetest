package test.tide.com.data.store;

import java.util.List;

import io.reactivex.Observable;
import test.tide.com.data.entity.GetPlacesResult;
import test.tide.com.data.entity.Place;

/**
 * Created by Lucian on 1/10/2017.
 */

public interface DataStore {
    Observable<GetPlacesResult> fetchPlaces(double latitude, double longitude, double radius, String type, String apiKey);
}
