package test.tide.com.data;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import test.tide.com.data.cache.Cache;
import test.tide.com.data.entity.GetPlacesResult;
import test.tide.com.data.entity.Place;
import test.tide.com.data.repository.PlacesRepository;
import test.tide.com.data.store.DataStore;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Lucian on 1/12/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class PlacesRepositoryTest {
    @Mock
    DataStore dataStore;
    @Mock
    Cache cache;

    private PlacesRepository repository;
    private List<Place> cachedPlaces = new ArrayList<>();
    private List<Place> dataStorePlaces = new ArrayList<>();

    private TestObserver<List<Place>> testSubscriber;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        repository = new PlacesRepository(dataStore, cache);
        testSubscriber = new TestObserver<>();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testReadFromCache() {
        when(cache.getPlaces(any(Double.class), any(Double.class), any(Double.class), any(String.class)))
                .thenReturn(cachedPlaces);
        repository.getPlaces(0, 0, 0, "", "").subscribeWith(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();
        testSubscriber.assertResult(cachedPlaces);

        verify(dataStore, never()).fetchPlaces(any(Double.class), any(Double.class), any(Double.class), any(String.class), any(String.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testReadFromDataStore() {
        GetPlacesResult dataStoreResult = new GetPlacesResult();
        dataStoreResult.setPlaces(dataStorePlaces);

        when(cache.getPlaces(any(Double.class), any(Double.class), any(Double.class), any(String.class)))
                .thenReturn(null);
        when(dataStore.fetchPlaces(any(Double.class), any(Double.class), any(Double.class), any(String.class), any(String.class)))
                .thenReturn(Observable.just(dataStoreResult));

        repository.getPlaces(0, 0, 0, "", "").subscribeWith(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();
        testSubscriber.assertResult(dataStorePlaces);

        verify(cache, times(1)).storePlaces(0, 0, 0, "", dataStorePlaces);
    }
}
